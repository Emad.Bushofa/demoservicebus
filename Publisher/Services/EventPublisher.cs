﻿using Azure.Messaging.ServiceBus;
using System.Text;
using System.Text.Json;

namespace Publisher.Services
{
    public class EventPublisher
    {
        private readonly ServiceBusSender _sender;

        public EventPublisher(ServiceBusClient serviceBusClient)
        {
            _sender = serviceBusClient.CreateSender("demo-customers");
        }

        public async Task PublishEventAsync(CustomerEvent customerEvent)
        {
            var json = JsonSerializer.Serialize(customerEvent);

            await _sender.SendMessageAsync(new ServiceBusMessage()
            {
                Body = new BinaryData(Encoding.UTF8.GetBytes(json)),
                PartitionKey = customerEvent.Id.ToString(),
                SessionId = customerEvent.Id.ToString(),
                Subject = customerEvent.Type
            });
        }
    }
}
